import React from 'react';
import Header from '../../../components/Footer';
import Footer from '../../../components/Header';

const MainLayout = Component => props => (
  <>
    <Header />
    <Component {...props} />
    <Footer />
  </>
);

export default MainLayout;
