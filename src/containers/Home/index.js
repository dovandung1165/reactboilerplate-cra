/* eslint-disable react/prefer-stateless-function */
/**
 *
 * Home
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

class Home extends React.Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Home</title>
          <meta name="description" content="Description of Home" />
        </Helmet>
        <div>This is homepage</div>
        {/* <FormattedMessage {...messages.header} /> */}
      </>
    );
  }
}

Home.propTypes = {};

export default Home;
