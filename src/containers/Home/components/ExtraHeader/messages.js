/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'portal.components.header';

export default defineMessages({
  extraHeaderTitle: {
    id: `${scope}.extra_header.title`,
    defaultMessage: '3000 great jobs for devs',
  },
  authorMessage: {
    id: `${scope}.author.message`,
    defaultMessage: `
      Made with love by {author}.
    `,
  },
});
