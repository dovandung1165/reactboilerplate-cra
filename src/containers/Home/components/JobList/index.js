import PropTypes from 'prop-types';
import styled from 'styled-components';
import React from 'react';

import _ from 'lodash';
import Job from '../Job';

const JobListWrapper = styled.div``;

function Joblist(props) {
  const { jobs } = props;
  return (
    <JobListWrapper>
      {!_.isEmpty(jobs) &&
        jobs.map((job, ind) => (
          <Job key={`found-job-${job.jobId}`} job={job} />
        ))}
    </JobListWrapper>
  );
}

Joblist.propTypes = {
  jobs: PropTypes.any,
};

export default React.memo(Joblist);
