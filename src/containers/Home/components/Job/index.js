import PropTypes from 'prop-types';
import styled from 'styled-components';
import React from 'react';

import { Row, Col } from 'reactstrap';

import _ from 'lodash';

const JobWraper = styled.div`
  border-bottom: 2px solid #e7e7e7;
  padding: 10px;
  background: white;
  box-shadow: 0 0 1px 1px lightgray;
  :hover {
    box-shadow: 0 0 5px 2px lightgray;
    transform: translate(0, -5px);
    z-index: 10;
  }
`;

const Avatar = styled.div``;
const CompanyName = styled.div``;
const Jobtitle = styled.div``;

function Header({ job }) {
  const benefits = _.get(job, 'benefits');
  return (
    <JobWraper>
      <Row>
        <Col xs={4}>
          <CompanyName>{job.company}</CompanyName>
          <Avatar>
            <img src={job.companyLogo} alt="company-logo" />
          </Avatar>
        </Col>
        <Col xs={8}>
          <Jobtitle>{job.jobTitle}</Jobtitle>
          <ul>
            {benefits.map((benefit, idx) => (
              <li key={`benefit-${idx}`}>{_.get(benefit, 'benefitName')}</li>
            ))}
          </ul>
        </Col>
      </Row>
    </JobWraper>
  );
}

Header.propTypes = {
  // jobs: PropTypes.any,
};

export default React.memo(Header);
